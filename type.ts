
export interface Article {
    id: number;
    urlToImage: string;
    title: string;
    description: string;
    author: string;
    publishedAt: string;
    content: string;
  }

  export interface DateType {
    date: DateType | null
  }