# News Page Aplication

This project is a simple news page application built using Next.js. It allows users to browse and filter news articles based on keywords, dates, and pagination. Each news item displays essential information such as author, title, description, URL to image, and publication date. Additionally, clicking on a news item opens a modal with detailed information.

## Table of Contents

- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
  - [Dependencies](#dependencies)
- [Usage](#usage)
- [Technologies Used](#technologies-used)
- [API Integration](#api-integration)
- [Pagination](#pagination)
- [Modal/Popup](#modalpopup)
- [Contributing](#contributing)

## Getting Started

### Prerequisites

Make sure you have the following installed on your machine:

- Node.js
- npm or yarn

### Installation

1. Clone the repository:

git clone https://gitlab.com/fahmihidayat/news-app.git

2. Navigate to the project directory:

cd news-page

3. Install dependencies:

npm install
or
yarn install

### Dependencies

Make sure you have the following dependencies installed in your project:

"dependencies": {
  "@heroicons/react": "^2.1.1",
  "@material-tailwind/react": "^2.1.8",
  "@tailwindcss/line-clamp": "^0.4.4",
  "axios": "^1.6.5",
  "next": "14.0.4",
  "react": "^18",
  "react-dom": "^18"
},
"devDependencies": {
  "@types/node": "^20",
  "@types/react": "^18",
  "@types/react-dom": "^18",
  "autoprefixer": "^10.0.1",
  "eslint": "^8",
  "eslint-config-next": "14.0.4",
  "postcss": "^8",
  "tailwindcss": "^3.3.0",
  "typescript": "^5"
}

To install these dependencies, run the following command in your project directory:

npm install
or
yarn install

This will ensure that all the required packages are installed and your project is set up correctly.

## Usage

1. Run the development server:

npm run dev
or
yarn dev

2. Open your browser and visit http://localhost:3000 to view the news page application.

## Technologies Used

- Next.js
- React
- TailwindCSS
- TypeScript


## API Integration
The application fetches news data from a NewsAPI. The utils/api.js file contains functions for making API requests.

## Pagination
The pagination section allows users to navigate through different pages of news articles.

## Modal/Popup
Clicking on a news item opens a modal displaying detailed information about the news article.

## Contributing
Feel free to contribute to the project by opening issues or submitting pull requests.