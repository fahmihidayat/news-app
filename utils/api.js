import axios from 'axios'

const apiKey = "ce24aecc12d14244b65ed64446a6981c";
const apiUrl = "https://newsapi.org/v2/";

export const getNews = async (
  keyword,
  startDate,
  endDate,
  language,
  pageSize,
  page,
  sources
) => {
  const params = {
    sources: sources || 'cnn, bbc-news, abc-news, bbc-sport, buzzfeed, cbc-news, espn, google-news, reuter, techcrunch',
    q: keyword,
    page: page || 1,
    pageSize: pageSize || 100,
    language: language || 'en'
  };

  if (startDate) {
    params.from = startDate;
  }

  if (endDate) {
    params.to = endDate;
  }

  try {
    const response = await axios.get(`${apiUrl}everything`, {
      params,
      headers: {
        "X-Api-Key": apiKey,
      },
    });

    return response.data.articles.map((article) => ({
      id: article.id,
      title: article.title,
      description: article.description,
      author: article.author,
      urlToImage: article.urlToImage,
      publishedAt: article.publishedAt,
      content: article.content,
    }));
  } catch (error) {
    console.error("error fetching news", error);
    return [];
  }
};
