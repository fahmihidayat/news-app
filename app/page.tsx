import NewsList from "./components/NewsList/NewsList";

export default function Home() {
  return (
    <>
      <main className="max-w-6xl mx-auto">
        <NewsList />
      </main>
    </>
  );
}
