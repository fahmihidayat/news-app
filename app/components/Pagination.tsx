import { IconButton, Typography } from "@material-tailwind/react";
import { ChevronDoubleLeftIcon, ChevronDoubleRightIcon } from "@heroicons/react/24/outline";

interface Props {
    currentPage: number;
    totalPages: number;
    handlePageChange: (page: number) => void
}

const Pagination = ({currentPage, totalPages, handlePageChange}: Props) => {
  return (
    <div className="pagination-controls mt-5 flex items-center justify-center gap-4">
        <IconButton
          size="sm"
          variant="outlined"
          onClick={() => handlePageChange(currentPage - 1)}
          disabled={currentPage === 1}
          placeholder={""}
          style={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end'}}
        >
          <ChevronDoubleLeftIcon strokeWidth={2} className="h-4 w-4" title="Previous" />
        </IconButton>
        <Typography color="gray" className="font-normal" placeholder={""}>
          Page <strong className="text-gray-900">{currentPage}</strong> of <strong className="text-gray-900">{totalPages}</strong>
        </Typography>
        <IconButton
          size="sm"
          variant="outlined"
          onClick={() => handlePageChange(currentPage + 1)}
          disabled={currentPage === totalPages}
          placeholder={""}
          style={{ display: 'flex', alignItems: 'center'}}
        >
          <ChevronDoubleRightIcon strokeWidth={2} className="h-4 w-4" title="Next" />
        </IconButton>
      </div>
  )
}

export default Pagination