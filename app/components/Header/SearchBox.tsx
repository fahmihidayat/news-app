'use client'

import { FormEvent, useState } from "react"

interface Props {
  onSearch: (keywords: string) => void
}

const SearchBox = ({onSearch}: Props) => {
  const [input, setInput] = useState("");

  const handleSearch = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if(!input) return;
    
    onSearch(input);
  }

  return <form 
  onSubmit={handleSearch}
  className="flex flex-col md:flex-row justify-between items-center gap-2 md:gap-4 max-w-xl px-10 md:px-0">
    <input type="text"
        value={input}
        onChange={(e) => setInput(e.target.value)}
        placeholder="Search Keywords..."
        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg
        focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
    />
    <button 
        type="submit"
        disabled={!input}
    >
    Search
    </button>    
  </form>
}

export default SearchBox