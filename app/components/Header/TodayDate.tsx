import { CopyOptions } from 'fs';
import React from 'react';

interface Options {
  weekday: 'long'; 
  month: 'long';
  day: 'numeric'; 
  year: 'numeric'; 
}

const TodayDate = () => {
  
  const today = new Date();

  const options: Options = { weekday: 'long', month: 'long', day: 'numeric', year: 'numeric'}

  const formattedDate = today.toLocaleDateString(undefined, options);

  return (
    <div className='text-center pt-4 pb-0 border-b-2'>
      <p>{formattedDate}</p>
    </div>
  );
};

export default TodayDate;