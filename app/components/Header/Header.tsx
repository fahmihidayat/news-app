import Link from "next/link"
import styles from "./Header.module.css"
import TodayDate from "./TodayDate"

const Header = () => {
  return <header>
    <TodayDate />
    <div className="p-10 items-center">
        <Link href='/' >
            <h1 className={`text-4xl text-center underline decoration-6 decoration-orange-400 ${styles.headerText}`}>
              Binar News
            </h1>
        </Link>
    </div>
  </header>
}

export default Header