"use client";

import { useState } from "react";
import { Article } from "../../type";

interface Props {
  article: Article;
}

const ReadMoreButton = ({ article }: Props) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleOpenModal = () => {
    setIsModalOpen(true);
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
  };

  return (
    <div>
      <button
        onClick={handleOpenModal}
        className="btn bg-orange-400 text-white active:bg-orange-500 font-bold uppercase 
         text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none 
         ease-linear transition-all duration-150 w-full"
      >
        Read Full Article
      </button>

      {isModalOpen && (
        <>
          <div
            className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed 
            inset-0 z-50 outline-none focus:outline-none"
          >
            <div className="relative w-auto my-6 mx-auto max-w-3xl">
              {/*content*/}
              <div
                className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white 
              outline-none focus:outline-none"
              >
                {/*header*/}
                <figure>
                  {article.urlToImage && (
                    <img
                      src={article.urlToImage}
                      alt={article.title}
                      className="h-100 w-full object-cover rounded-t-lg shadow-md"
                    />
                  )}
                </figure>
                <div
                  className="flex flex-col p-5 border-b border-solid 
                border-blueGray-200 rounded-t"
                >
                  <h3 className="text-3xl font-semibold font-serif">{article.title}</h3>
                  <div className="text-xs pt-5 italic text-gray-500 mt-auto">
                    <p>Author: {article.author || "unknown"}</p>
                    <p>Published At: {new Date(article.publishedAt).toLocaleDateString()}</p>
                  </div>
                </div>
                {/*body*/}
                <div className="relative p-6 flex-auto">
                  <p className="my-4 text-blueGray-500 text-lg leading-relaxed font-serif">
                    {article.content}
                  </p>
                </div>
                {/*footer*/}
                <div
                  className="flex items-center justify-end p-6 border-t border-solid 
                border-blueGray-200 rounded-b"
                >
                  <button
                    className="text-red-500 background-transparent font-bold uppercase 
                    px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                    type="button"
                    onClick={handleCloseModal}
                  >
                    Close
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      )}
    </div>
  );
};

export default ReadMoreButton;
