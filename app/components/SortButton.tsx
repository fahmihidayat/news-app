import React from 'react';
import { ArrowsUpDownIcon } from '@heroicons/react/24/outline';

interface Props {
  onSortToggle: () => void;
}

const SortButton = ({ onSortToggle }: Props) => {
  return (
    <button onClick={onSortToggle} title='Sort by Date'>
      <ArrowsUpDownIcon className="inline-block h-8 w-8" />
    </button>
  );
};

export default SortButton;