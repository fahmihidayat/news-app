"use client";

import { useState } from "react";
import ReadMoreButton from "../ReadMoreButton";
import { Article } from "../../../type";
import Pagination from "../Pagination";

interface Props {
  articles: Article[];
}

const NewsItem = ({ articles }: Props) => {
  const pageSize = 10;
  const [currentPage, setCurrentPage] = useState(1);

  const startIndex = (currentPage - 1) * pageSize;
  const endIndex = startIndex + pageSize;

  const currentArticles = articles.slice(startIndex, endIndex);
  const totalPages = Math.ceil(articles.length / pageSize);

  const handlePageChange = (page: number) => {
    setCurrentPage(page);
  };
  return (
    <>
      <Pagination currentPage={currentPage} totalPages={totalPages} handlePageChange={handlePageChange} />
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-10 p-10">
        {currentArticles.map((article) => (
          <div
            key={article.id}
            className="card lg:card-side flex flex-col flex-1 bg-base-100 shadow-xl w-full rounded-lg"
          >
            <figure>
              {article.urlToImage && (
                <img
                  src={article.urlToImage}
                  alt={article.title}
                  className="h-56 w-full object-cover rounded-t-lg shadow-md"
                />
              )}
            </figure>
            <div className="card-body flex flex-1 flex-col p-5">
              <h3 className="card-title font-bold font-serif">
                {article.title}
              </h3>
              <section className="flex-1 mt-2">
                <p className="text-xs line-clamp-2">{article.description}</p>
              </section>
              <footer className="text-xs pt-5 italic text-gray-500 mt-auto">
                <p>Author: {article.author || "unknown"}</p>
                <p>
                  Published At:{" "}
                  {new Date(article.publishedAt).toLocaleDateString()}
                </p>
              </footer>
            </div>
            <ReadMoreButton article={article} />
          </div>
        ))}
      </div>
      {/* Pagination Controls */}
      <Pagination currentPage={currentPage} totalPages={totalPages} handlePageChange={handlePageChange} />
    </>
  );
};

export default NewsItem;
