"use client";

import { useState, useEffect, FormEvent } from "react";
import { getNews } from "../../../utils/api"
import NewsItem from "./NewsItems";
import SearchBox from "../Header/SearchBox";
import { Article } from "@/type";
import SortButton from "../SortButton";
import DateFilter from "./DateFilter";

const NewsList = () => {
  const [news, setNews] = useState<Article[]>([]);
  const [searchKeywords, setSearchKeywords] = useState("");
  const [sortBy, setSortBy] = useState("publishedAt");
  const [startDate, setStartDate] = useState<Date | null>(null);
  const [endDate, setEndDate] = useState<Date | null>(null);

  const handleSortToggle = () => {
    setSortBy((prevSortBy) =>
      prevSortBy === 'publishedAt' ? '-publishedAt' : 'publishedAt'
    );

    setNews((prevNews) =>
      prevNews.slice().sort((a, b) => {
        if (sortBy === 'publishedAt') {
          return new Date(a.publishedAt).getTime() - new Date(b.publishedAt).getTime();
        } else {
          return new Date(b.publishedAt).getTime() - new Date(a.publishedAt).getTime();
        }
      })
    );
  };

  const handleSearch = (keywords: string) => {
    setSearchKeywords(keywords)
  }

  const handleDateChange = (startDate: Date | null, endDate: Date | null) => {
    setStartDate(startDate)
    setEndDate(endDate)
  }

  useEffect(() => {
    const fetchNews = async () => {
      const articles = await getNews(searchKeywords || undefined, startDate || null, endDate || null);
      setNews(articles);
    };

    fetchNews();
  }, [searchKeywords, startDate, endDate]);

  return (
    <div className="my-5">
      <div className="grid grid-cols-1 md:grid-cols-2 justify-center md:justify-between items-center mb-6 gap-4 md:gap-10">
        <SearchBox onSearch={handleSearch}/>
        <DateFilter onDateChange={handleDateChange}/>
      </div>
      <div className="flex justify-between border-b-2 mb-4 pb-2 px-4 md:px-0">
        <div className="text-2xl underline decoration-orange-400 decoration-6 capitalize">
        {searchKeywords !== ""
            ? `Search results for: ${searchKeywords}`
            : 'Latest News'}
        </div>
        <SortButton onSortToggle={handleSortToggle} />
      </div>
      {news.length === 0 ? (
        <p className="text-2xl text-center">No results found for '{searchKeywords}'.</p>
      ) : (
        <NewsItem articles={news} />
      )}
    </div>
  );
};

export default NewsList;