import React, { useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendarAlt, faFilter } from '@fortawesome/free-solid-svg-icons';

interface Props {
  onDateChange: (fromDate: Date | null, toDate: Date | null) => void
}

const NewsFilter = ({ onDateChange }: Props) => {
  
  const [fromDate, setFromDate] = useState<Date | null>(null);
  const [toDate, setToDate] = useState<Date | null>(null);

  const handleFromDateChange = (date: Date | null) => {
    setFromDate(date);
  };

  const handleToDateChange = (date: Date | null) => {
    setToDate(date);
  };

  const handleFilter = () => {
    if (fromDate !== null && toDate !== null) {
      onDateChange(fromDate, toDate);
    }
  };

  return (
    <div className="flex flex-col md:flex-row items-center md:justify-between gap-2">
      <div className="date-picker-container">
        <label className='flex gap-2 items-center'>
          <FontAwesomeIcon icon={faCalendarAlt} />
          <DatePicker
          key="fromDate" 
          selected={fromDate} 
          onChange={handleFromDateChange}
          placeholderText='from'
          className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg
          focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5'/>
        </label>
      </div>
      <div className="date-picker-container">
        <label className='flex gap-2 items-center'>
          <FontAwesomeIcon icon={faCalendarAlt} />
          <DatePicker
          key="toDate" 
          selected={toDate} 
          onChange={handleToDateChange}
          placeholderText='to'
          className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg
          focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5' />
        </label>
      </div>
      <button onClick={handleFilter}>
        <FontAwesomeIcon icon={faFilter} />
      </button>
    </div>
  );
};

export default NewsFilter;
